// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter_web/material.dart';
import 'package:http/http.dart' as http;
import 'const/resource.dart';
import 'dart:html' as html;

import 'lib.dart';
import 'local_storage.dart';

void main() {
  runApp(MyApp());
  print("1 is int : ${1 is int}"); // true
  print("1 is double : ${1 is double}"); // true
  print("1.0 is int : ${1.0 is int}"); // true
  print("1.0 is double : ${1.0 is double}"); // true

  print(1.runtimeType); // int
  print(1.0.runtimeType); // int

  var value = LocalStorage.getItem("key");
  print("从 local storage 中取出来的 $value");

  LocalStorage.setItem("key", "我是要保存的信息");
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int counter = 0;
  TextEditingController controller = TextEditingController();
  var key = GlobalKey();
  void add() {
    counter++;
    setState(() {});
    // ScaffoldState state = key.currentState;
    // state.showSnackBar(
    //   SnackBar(
    //     content: Text(controller.text),
    //   ),
    // );
  }

  static var divider = Container(
    padding: const EdgeInsets.symmetric(vertical: 10),
    child: Text("我是分割线"),
    decoration: BoxDecoration(
      border: Border.all(
        color: Colors.blue,
        width: 5,
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: key,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            TextField(
              controller: controller,
            ),
            Text(
              counter.toString(),
            ),
            // Image.network(
            //     "https://raw.githubusercontent.com/kikt-blog/image/master/img/20190508104658.png"),
            divider,
            // Image.asset(R.IMG_20190508104658_PNG),
            Container(
              width: 300,
              height: 300,
              child: Image.asset(R.IMG_20190508104658_PNG),
            ),
            divider,
            fromHttp(),
            divider,
            _buildHtmlFunWidget(),
            divider,
            _callJSMethodWidget(),
            // Image.memory(getImageList(imageBase64)),
          ],
          // scrollDirection: Axis.horizontal,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: add,
        tooltip: 'push',
        child: Icon(Icons.add),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    print("${this.runtimeType} initState");
  }

  @override
  void dispose() {
    print("${this.runtimeType} dispose");
    super.dispose();
  }

  fromHttp() {
    return FutureBuilder<http.Response>(
      future: http.get("https://api.github.com/"),
      builder: (BuildContext context, snapshot) {
        if (!snapshot.hasData) {
          return Container();
        }
        var body = snapshot.data.body;
        return Text(body);
      },
    );
  }

  _buildHtmlFunWidget() {
    return FlatButton(
      child: Text("操作html元素"),
      onPressed: () {
        var e = html.window.document.getElementById("tmpId");
        e.text = this.counter.toString();
      },
    );
  }

  _callJSMethodWidget() {
    return FlatButton(
      child: Text("点击"),
      onPressed: () {
        add1And2();
        // print(getFromJS());
        print(getJS());
      },
    );
  }
}
