@JS()
library lib;

import 'package:js/js.dart';

@JS()
external String getFromJS();

@JS()
external num addNumbers(num a, num b);

void add1And2() {
  print(addNumbers(1, 2));
}

String getJS() {
  return getFromJS();
}
