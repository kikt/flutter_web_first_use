import 'dart:convert';
import 'dart:io';

import 'package:flutter_web.examples.hello_world/const/resource.dart';

void main() {
  var file = File(R.IMG_20190508104658_PNG);
  print(file.lengthSync());

  var encodedStr = base64.encode(file.readAsBytesSync());
  var f = File("img/base64.txt");
  f.writeAsStringSync(encodedStr);
}
